import java.util.Scanner;

public class Calculator {

    static int intIn;
    static int numOne;
    static int numTwo;
    static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {

        System.out.println("Меню:\n1 - сложим 2 числа\n2 - из первого числа вычтем второе\n3 - умножим 2 числа\n4 - первое число поделим на второе");

        while (true) {
            intIn = scanner.nextInt();
            if (intIn == 1) {
                System.out.println("Введите первое и второе число");
                numOne = scanner.nextInt();
                numTwo = scanner.nextInt();
                System.out.println(numOne + "+" + numTwo + "=" + numOne+numTwo);
            } else if (intIn == 2) {
                System.out.println("Введите первое и второе число");
                numOne = scanner.nextInt();
                numTwo = scanner.nextInt();
                int minus = numOne-numTwo;
                System.out.println(numOne + "-" + numTwo + "=" + minus);
            } else if (intIn == 3) {
                System.out.println("Введите первое и второе число");
                numOne = scanner.nextInt();
                numTwo = scanner.nextInt();
                System.out.println(numOne + "*" + numTwo + "=" + numOne*numTwo);
            } else if (intIn == 4) {
                try {
                    System.out.println("Введите первое и второе число");
                    numOne = scanner.nextInt();
                    numTwo = scanner.nextInt();
                    if (numOne % numTwo == 0) {
                        System.out.println(numOne + "/" + numTwo + "=" + (numOne/numTwo));
                    } else {
                        double division = (double)numOne/(double)numTwo;
                        System.out.println(numOne + "/" + numTwo + "=" + division);
                    }
                } catch (ArithmeticException e) {
                    System.out.println("На 0 делить нельзя!!! Ты сломал программу! Я отказываюсь работать!");
                    scanner.close();
                    scanner=null;
                    break;
                }

            } else System.out.println("Мы о таком не договаривались!");
        }
    }
}